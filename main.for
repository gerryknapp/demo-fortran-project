
      program sampleGit

c     Declare variables
      character(LEN=20) :: msg, err
      character(LEN=32) :: arg, infile,fname
      real, dimension(100) :: var
      real :: sumv, avgv, minv, maxv
      integer :: mino, maxo
      integer :: i, io, indx, cnt, argcnt

c     Output a banner
      write(6,*)
      write(6,'(A)') '**********************************************'
      write(6,'(A)') '*                                            *'
      write(6,'(A)') '*            A Fortran Program               *'
      write(6,'(A)') '*                                            *'
      write(6,'(A)') '*   Originally written by Gerald L. Knapp    *'
      write(6,'(A)') '*                   (2019)                   *'
      write(6,'(A)') '*                                            *'
      write(6,'(A)') '**********************************************'
      write(6,*)

c     Default values
      infile='input.txt'

c     Parse command line arguments
      argcnt=command_argument_count()
      do i=1,argcnt
         call get_command_argument(i, arg)
         select case (arg)
         case ('-n','--name')
            if (i+1.le.argcnt) then
               call get_command_argument(i+1, infile)
            else
               write(6,'(2A)') 'Please enter a valid
     $ file name following ', arg
            endif
         case ('-h','--help')
            write(6,'(/,A)') 'Available commands:'
            write(6,'(A)') '   -n, --name      Specify custom
     $ file name for input file.'
            write(6,'(A)') '                   File name is
     $ relative to current directory.'
            write(6,'(A)') '                   (e.g., `-n custom.txt`
     $ reads `./custom.txt`)'
            write(6,'(A)') '                   Default name is
     $ `input.txt`.'
         end select
      enddo

c     Read variables from the input file
c     Continue reading in values until the end of file is reached
c     Ignores blank lines and assumes no variable value is equal to zero
      var(:)=0.
      fname='./inputs/' // infile
      open(unit=10, file=fname)
      io=0
      cnt=0
      indx=1
      do while (io.eq.0)
         read(10,*, IOSTAT=io) var(indx)
         if (var(indx).ne.0.) cnt=cnt+1
         indx=indx+1
         if (io > 1) then
            write(6,*) 'Error reading input.'
            write(6,*) 'Aborting.'
            stop
         endif
      enddo
      close(10)

c     Analyze input values
      mino=100
      maxo=-100
      sumv=sum(var(1:cnt))
      avgv=sum(var(1:cnt)) / cnt
      minv=minval(var(1:cnt))
      maxv=maxval(var(1:cnt))
      write(6,110) ''
      do i=1,cnt
         write(6,120) 'Value of variable ',i,': ', var(i)
         mino=min(mino, nint(log10(var(i))))
         maxo=max(maxo, nint(log10(var(i))))
      enddo
      write(6,110) ''
      write(6,110) 'Analysis of the variables:' 
      write(6,100) '   Sum: ', sumv
      write(6,100) '   Average: ', avgv
      write(6,100) '   Minimum: ', minv
      write(6,100) '   Maximum: ', maxv
      write(6,130) '   Smallest order of magnitude: 1e', mino
      write(6,130) '   Largest order of magnitude: 1e', maxo

c     Output data to CSV file
      open(unit=10, file='./results.csv')
      write(10,110) 'Sum, Average, Minimum, Maximum'
      write(10,140) sumv, avgv, minv, maxv
      close(10)
c     Define write formats
100   format(A, ES10.3E2)
110   format(A)
120   format(A,i0,A,ES10.3E2)
130   format(A, i0)
140   format(4(ES10.3E2,', '))

      end program sampleGit