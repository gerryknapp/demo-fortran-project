# Demo Fortran Project

Demonstration of using git for a private code repository. Sample code is written
in fixed-format Fortran.

## Overview

This program is very simple. It reads in data from `input.txt`, then does some
basic analysis on the provided variables.

Despite its simplicity, this project demonstrates the utility of using a git
for version control. Version control refers to the system of managing software
projects. Git enables several key features of version control, including
* tracking where changes were made in the code
* associating all changes made with a user profile
* allowing for separate users to develop "branches" of the software in parallel
* providing tools to merge separate branches of development into the main branch

Because this project has an input file with an assumed format, tracking 
changes via git is important. If the input file format changes, then it
is important to understand what other changes to the code were made in
order to read the new file format. For example, look at commit
https://gitlab.com/gerryknapp/demo-fortran-project/commit/5ed84cdd8765f037eb71e8b4bce05cf65bce76e4.
There, the input file format was changed, and it is clear from looking
at the other changes that the reading of the file format was changed
appropriately. It is also apparent what is done with the new variable inside
of the program.

## Tools for using git
Essentially, git provides a tool to record changes to the code. In scientific
code development, it serves as a digital lab notebook. Services like GitLab and
GitHub have built graphical web-based interfaces to manage, host, and interact
with git repositories. While both GitLab and GitHub store copies of git
repositories on their own servers, neither have any license or claim to 
intellectual property for repositories they host. GitLab makes its profits from
large, corporate users that require more complex development tools, so users
can have an unlimited number of free, private
repositories with unlimited collaborators and basic functionality. GitHub
only offers free repositories with unlimited collaborators for publicly-shared
source code. Though GitHub does also provide individual users with a limited
number of free private repositories, these private repositories can only have
limited number of collaborators. 

Due to its business model, GitLab is perfectly suited for scientific code
development. Many scientific codes are perpetually "in development", making the 
openness of GitHub unsuitable. Private repositories allow for strong control
of who has access to the project, and access can be added or revoked by the
project manager at any time. Additionally, access permissions can be set
per user, such that some users can both download the source code and 
upload changes, while other users may be limited to just downloading the code.


 
